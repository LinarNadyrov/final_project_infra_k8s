#### Здравствуй уважаемый читатель. 
Содержащая далее информация, это моя проектная работа по [курсу](https://otus.ru/lessons/infrastrukturnaya-platforma-na-osnove-kubernetes/?int_source=courses_catalog&int_term=operations). 

#### В ходе проектой работы реализовано: 
1. Развертывание инфраструктуры на основе Kubernetes (~~managed [GKE](https://cloud.google.com/kubernetes-engine)~~  [Yandex Managed Service for Kubernetes](https://cloud.yandex.ru/services/managed-kubernetes)). 
2. Мониторинг кластера и приложений с алертами и нужными дашбордами.
3. Централизованное логирование для кластера и приложений.
4. Развертывание микросервисного приложения ([hipster-shop](https://github.com/GoogleCloudPlatform/microservices-demo)) с инструментами helm, helmfile, GitOps ([Flux](https://github.com/fluxcd/flux))
5. С помощью CI/CD пайплайн ([GitLab](https://about.gitlab.com/))
----

[Подробнее](https://gitlab.com/LinarNadyrov/final_project_infra_k8s/-/blob/master/infra/README.md) про создание инфраструктуры.

[Подробнее](https://gitlab.com/LinarNadyrov/final_project_infra_k8s/-/blob/master/development/README.md) про развертывание приложения.

----
#### Выбранные мной компоненты
`frontend` (Go) - Предоставляет HTTP-сервер для обслуживания веб-сайта. Не требует регистрации / входа в систему и автоматически генерирует идентификаторы сеанса для всех пользователей.

`cartservice` (C#) - Сохраняет товары в корзине покупателя в Redis и извлекает их.

`productcatalogservice` (Go) - Предоставляет список продуктов из файла JSON и возможность поиска продуктов и получения отдельных продуктов. 

`recommendationservice` (Python) - Рекомендует другие товары на основе того, что указано в корзине.

`currencyservice` (Node.js) - Конвертирует одну денежную сумму в другую валюту. Использует реальные ценности, полученные от Европейского центрального банка. Это самый высокий сервис QPS.

`checkoutservice` (Go) - Извлекает тележку пользователя, подготавливает заказ и организует оплату, доставку и уведомление по электронной почте. 

`shippingservice` (Go) - Предоставляет оценку стоимости доставки на основе корзины покупок. Отправляет товары по указанному адресу (макет)

----
#### Последовательность развертывания app
- Cashe(redis)          - redis.yaml 
- CartService           - cartservice.yaml
- CheckoutService       - checkoutservice.yaml

- RecomendationService  - recommendationservice.yaml
- ProductCatalogService - productcatalogservice.yaml
- CurrencyService       - currencyservice.yaml
- ShippingService       - shippingservice.yaml

- Frontend              - frontend.yaml
----

А теперь то, что нужно описать и придерживаться к этому :-) 

### Задание: подготовка MVP инфраструктурной платформы для приложения-примера
- Цели проектной работы
- Требования к реализации
- Приемка и критерии
### Цели проектной работы 
- Закрепление и демонстрация полученных знаний и навыков
- Опыт командной работы (да, можно в командах по 2-3 человека)
- Портфолио для работодателя
- Продолжение обучения
- Опционально: представление работы на последнем вебинаре
### Организация работы
- Отдельный, публично доступный репозиторий
- Ссылка на репозиторий публикуется в Slack группы (до приемки)
- При желании порабоать в команде самоорганизоваться в чате
- Сертификат выдается после окончания приемки проектных работ
- Результат проектной работы должен соответстовать требованиям

----

#### Требования 1/5 | Функционал
#### MVP платформы обязательно включает в себя: 
- Kubernetes (managed или self-hosted)
- Мониторинг кластера и приложения с алертами и дашбордами
- Централизованное логирование для кластера и приложений
- CI/CD пайплайн для приложения (Gitlab CI, Github Actions, etc)
#### Дополнительно:
- Инфраструктура для трейсинга
- Хранилище артефактов
- Хранилище секретов

#### Требования 2/5 | Оформление
#### Обязательно в виде кода в репозитории
- Автоматизация создания и настройки кластера
- Развертывание и настройки сервисов платформы
- Настройки мониторинга, алерты, дашборды
- CI/CD пайплайн для приложения описан кодом

#### Требования 3/5 | Доступы
#### Мы должны иметь возможность проверить
- Публично доступный репозиторий с кодом и пайплайном
- Публично доступный ingress кластера (белый IP, домен)

#### Требования 4/5 | Документация
#### Что вы сделали и как это повторить
- README с описанием решения, changelog
- Шаги развертывания и настройки компонентов
- Описание интеграции с CI/CD-сервисом
- Подход к организации мониторинга и логирования
- Дополнительная информация про вашу платформу

#### Требования 5/5 | Приложение
#### Есть два пути
- Наш пример: [Sock Shop by Weaveworks](https://microservices-demo.github.io/)
- Приложение на ваш выбор
    + Opensource
    + Подходящаяя лицензия (MIT, Apache, etc)
    + Доступно по HTTP
    + Минимум три развертываемых компонента

----
### Критерии оценки
#### Что мы будем оценивать
- Автоматизация разертывания и настройки кластера
- Автоматизация развертывания и настройки экосистемы
- Реализация CI/CD для приложения
- Объем нового

#### Результат оценки
- Отменим сильные стороны
- Подсветим, что можно было сделать лучше

----
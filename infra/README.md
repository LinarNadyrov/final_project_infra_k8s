1. Подключаем GitLab Runner.
- Более подробно как подключить описано [тут](https://docs.gitlab.com/runner/install/).
- Характеристики подключаемой виртуальной машины:
`CPU 3 / Memory 4 Gb / SSD 30 Gb / OC: Ubuntu 18.04.5 LTS / Docker Engine Community Version: 19.03.13`

2. a) Настраиваем gcloud tool.
- Более подробно как это сделать описано [тут](https://cloud.google.com/kubernetes-engine/docs/quickstart#cloud-shell).
- Настройка проводится на той же виртуальной машине, где установлен GitLab Runner.

2. б) Настраиваем интерфейс командной строки Yandex.Cloud (CLI). 
- Более подробно можно прочитать [тут](https://cloud.yandex.ru/docs/cli/quickstart#install)
- Добавление учетных данных в конфигурационный файл kubectl можно прочитать [тут](https://cloud.yandex.ru/docs/managed-kubernetes/operations/kubernetes-cluster/kubernetes-cluster-get-credetials)
- Про создание кластера Kubernetes можно прочитать [тут](https://cloud.yandex.ru/docs/managed-kubernetes/operations/kubernetes-cluster/kubernetes-cluster-create)
- Про создание группу узлов можно прочитать [тут](https://cloud.yandex.ru/docs/managed-kubernetes/operations/node-group/node-group-create)

3. Весь процесс "раскатки" реализовал через CI/CD пайплайн GitLab'а. 
- Для этого создаем в корне репозитория файл [.gitlab-ci.yml](https://gitlab.com/LinarNadyrov/final_project_infra_k8s/-/blob/master/.gitlab-ci.yml).
- Наглядно CI/CD пайплайн можно посмотреть [тут](https://gitlab.com/LinarNadyrov/final_project_infra_k8s/-/pipelines).

4. a) Создаем k8s кластер в GCP (количество НОД может меняться - --num-nodes=4)
```
gcloud beta container clusters create otus-final-project --zone europe-west1-b --num-nodes=4 --machine-type=n1-standard-2 --disk-size=50GB --no-enable-stackdriver-kubernetes
```
Команда для удаления k8s
```
gcloud container clusters delete otus-final-project --zone europe-west1-b
```
4. б) Создаем k8s кластер в Yandex cloud (количество НОД может меняться - --num-nodes=4)
```
- yc managed-kubernetes cluster create --name otus-final-project --network-name default --zone ru-central1-a --subnet-name default-ru-central1-a --public-ip --release-channel regular --version 1.17 --cluster-ipv4-range 10.1.0.0/16 --service-ipv4-range 10.2.0.0/16 --service-account-name k8s-otus --node-service-account-name k8s-otus --daily-maintenance-window start=22:00,duration=10h

- yc managed-kubernetes node-group create --name node-group --fixed-size 3 --cluster-name otus-final-project --platform-id standard-v2 --location subnet-name=default-ru-central1-a,zone=ru-central1-a --public-ip --cores 4 --memory 6 --core-fraction 20 --preemptible --disk-type network-hdd --disk-size 64 --version 1.17 --daily-maintenance-window start=22:00,duration=10h

- yc managed-kubernetes cluster get-credentials otus-final-project --external --force
```


5. Установка ingress
```
createJob_ingress:
  stage: create-ingress
  tags:
    - final_project_infra_k8s
  script:
    # TODO дополнить ingress-nginx cloud dns и external dns
    - helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
    - helm repo update
    - kubectl apply -f $PATH_INGRESS/ns_ingress.yaml
    - helm upgrade --install ingress-nginx ingress-nginx/ingress-nginx --namespace=nginx-ingress --version=3.12.0 -f $PATH_INGRESS/nginx-ingress.values.yaml
    - kubectl get svc -A | grep ingress
  allow_failure: true
```
Описание полного pipeline можно посмотреть [.gitlab-ci.yml](https://gitlab.com/LinarNadyrov/final_project_infra_k8s/-/blob/master/.gitlab-ci.yml)

6. Реализуем мониторинг кластера
```
createJob_monitoring:
  stage: create-monitoring
  tags:
    - final_project_infra_k8s
  script:
    - helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    - helm repo add stable https://charts.helm.sh/stable
    - helm repo update
    - kubectl apply -f $PATH_MONITORING/ns_monitoring.yaml
    - helm upgrade --install prometheus prometheus-community/kube-prometheus-stack --namespace=monitoring -f $PATH_MONITORING/prometheus-operator.values.yaml
    - helm upgrade --install blackbox-exporter prometheus-community/prometheus-blackbox-exporter --namespace=monitoring -f $PATH_MONITORING/blackbox-exporter.values.yaml
    - kubectl get svc -A | grep ingress
```
Описание полного pipeline можно посмотреть [.gitlab-ci.yml](https://gitlab.com/LinarNadyrov/final_project_infra_k8s/-/blob/master/.gitlab-ci.yml)

7. Реализуем логирование кластера
```
createJob_logs:
  stage: create-logs
  tags:
    - final_project_infra_k8s
  when: manual
  except: 
    - tags
  script:
    - helm repo add elastic https://helm.elastic.co
    - helm repo add fluent-bit https://fluent.github.io/helm-charts
    - helm repo add stable https://charts.helm.sh/stable
    - helm repo update
    - kubectl apply -f $PATH_LOGGING/ns_logs.yaml
    - helm upgrade --install elasticsearch elastic/elasticsearch --namespace=logs -f $PATH_LOGGING/elasticsearch.values.yaml
    - helm upgrade --install kibana elastic/kibana --namespace=logs -f $PATH_LOGGING/kibana.values.yaml
    - helm upgrade --install fluent-bit fluent-bit/fluent-bit --namespace=logs -f $PATH_LOGGING/fluent-bit.values.yaml
    # install elasticsearch-exporter
    - helm upgrade --install elasticsearch-exporter stable/elasticsearch-exporter --set es.uri=http://elasticsearch-master:9200 --set serviceMonitor.enabled=true --namespace=logs
    - sleep 90
    - kubectl get node && kubectl get pods -A -o wide | grep fluent
    - echo " ============================================================================================================= "
    - kubectl get pods -n logs -o wide -l chart=elasticsearch
```
Описание полного pipeline можно посмотреть [.gitlab-ci.yml](https://gitlab.com/LinarNadyrov/final_project_infra_k8s/-/blob/master/.gitlab-ci.yml)

8. Реализуем GitOps.

Подробная реализация GitOps с [п.3](https://gitlab.com/LinarNadyrov/final_project_infra_k8s/-/blob/master/development/README.md).

9. Реализуем мониторинг app
10. Реализуем логирование app (если возможно) 



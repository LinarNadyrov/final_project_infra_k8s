Развертывание микросервисного приложения [hipster-shop](https://github.com/GoogleCloudPlatform/microservices-demo) реализовано с инструментами [helm](https://github.com/helm/helm), [helmfile](https://github.com/roboll/helmfile), GitOps ([Flux](https://github.com/fluxcd/flux))

Почему с 3-мя инструментами? Ответ на самом деле прост - хотелось поближе познакомится с каждым инструментом. 

1. Развертывание приложения с инструментом **helm**.

Ниже описан "кусок" кода с помощью которого деплоится приложение (app) cartservice. Описание полного pipeline можно посмотреть [.gitlab-ci.yml](https://gitlab.com/LinarNadyrov/final_project_infra_k8s/-/blob/master/.gitlab-ci.yml)

```
releaseJob-CartService:
  stage: release-CartService
  tags:
    - final_project_infra_k8s
  script:
    - kubectl apply -f development/ns_hipster-shop.yaml
    - helm upgrade --install cartservice development/charts/cartservice --namespace hipster-shop
```

2. Развертывание приложения с инструментом **helmfile**.

Ниже описан "кусок" кода с помощью которого деплоится множество приложений. Главное отличие от п.1 это [helmfile](https://gitlab.com/LinarNadyrov/final_project_infra_k8s/-/blob/master/development/helmfile.yaml) где описаны множество helm релизов. Описание полного pipeline можно посмотреть [.gitlab-ci.yml](https://gitlab.com/LinarNadyrov/final_project_infra_k8s/-/blob/master/.gitlab-ci.yml)

```
releaseJob-OtherServices:
  stage: release-OtherServices
  tags:
    - final_project_infra_k8s
  script:
    - kubectl apply -f development/ns_hipster-shop.yaml
    - cd development && helmfile repos
    - helmfile sync
```
3. Развертывание приложения с инструментом **Flux** (GitOps).

3.1. **GitOps. Установка Flux**

Установим CRD, добавляющую в кластер новый ресурс - HelmRelease:
```
kubectl apply -f infra/flux/crds.yaml
```
Добавим официальный репозиторий Flux:
```
helm repo add fluxcd https://charts.fluxcd.io
helm repo update
```
Произведем установку Flux в кластер, в нужный нам namespace:
```
kubectl apply -f $PATH_FLUX/ns_flux.yaml
helm upgrade --install flux fluxcd/flux --namespace=flux -f $PATH_FLUX/flux.values.yaml
```
Установим Helm operator%
```
helm upgrade --install helm-operator fluxcd/helm-operator --namespace=flux -f $PATH_FLUX/helm-operator.values.yaml
```
Получаем значение ssh-ключа следующей командой:
```
fluxctl identity --k8s-fwd-ns flux
```
Добавляем полученный ssh-ключ в наш репозиторий (делается пока только через UI)

3.2. На что следует обратить внимание

Файл [flux.values.yaml](https://gitlab.com/LinarNadyrov/final_project_infra_k8s/-/blob/master/infra/flux/flux.values.yaml)

Указываем для Flux каталог с которого происходит автоматическое синхронизация состояния кластера и репозитория. 

Файл [frontend.yaml](https://gitlab.com/LinarNadyrov/final_project_infra_k8s/-/blob/master/development/flux/frontend.yaml)

Описывается **HelmRelease** которым управляет helm-operator, в этом файле подробно изложена конфигурация релиза.

3.3. Tag для Flux

Если посмотрим tag в репозитории, то увидем: 
```
git tag --l
# вывод 
flux-sync 

git show flux-sync 
# вывод 
tag flux-sync
Tagger: Weave Flux <support@weave.works>
Date:   Sat Dec 19 11:11:56 2020 +0000

Sync pointer

commit 1b850306d554ea73ec1e9c3a0b160b6e650c72ec (tag: flux-sync)
Author: Weave Flux <support@weave.works>
Date:   Sat Dec 19 11:11:50 2020 +0000

    Auto-release linarnadyrov/frontend:v0.2.2

    [ci skip]

diff --git a/development/flux/frontend.yaml b/development/flux/frontend.yaml
index c1cabb9..ae15795 100644
--- a/development/flux/frontend.yaml
+++ b/development/flux/frontend.yaml
@@ -18,4 +18,4 @@ spec:
   values:
     image:
       repository: linarnadyrov/frontend
-      tag: v0.2.1
+      tag: v0.2.2
```